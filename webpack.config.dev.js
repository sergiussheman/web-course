const merge = require('webpack-merge');
const webpack = require('webpack');
const webpackConfig = require('./webpack.config');

module.exports = merge(webpackConfig, {

    mode: "development",
    devtool: 'cheap-eval-source-map',
    devServer: {
        watchContentBase: true
    },
    output: {
        pathinfo: true,
        publicPath: '/',
        filename: '[name].js'
    },
    module: {
        rules: [
            // STYLES
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },
            // HTML
            {
                test: /\.(html)$/,
                use: 'html-loader'
            },
            // IMAGES
            {
                test: /\.(jpe?g|png|gif)$/,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]'
                }
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            IS_DEV: process.env.NODE_ENV === 'dev'
        })
    ]
});
