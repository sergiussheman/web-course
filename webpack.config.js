const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');

/**
 * Webpack Configuration
 */
module.exports = {
    target: "web",
    entry: [
        './app/index.js'
    ],
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(__dirname, 'index.html'),
            inject: true,
            inlineSource: 'sass-loader!style-loader!.s?css$'
        }),
        new HtmlWebpackInlineSourcePlugin()
    ]
};
